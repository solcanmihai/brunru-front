import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Job } from '../jobs/jobs.component';

@Component({
  selector: 'app-application-form',
  templateUrl: './application-form.component.html',
  styleUrls: ['./application-form.component.scss']
})
export class ApplicationFormComponent implements OnInit {

  constructor(private apiService: ApiService, private router: Router, @Inject(MAT_DIALOG_DATA) public data: Job, public dialogRef: MatDialogRef<ApplicationFormComponent>) { }

  fileToUpload: any;

  ngOnInit(): void {
  }

  sendMessage(prenume: string, nume: string, email: string, telefon: string, mesaj: string
  ) {
    const formData = new FormData();
    if (prenume)
    formData.append('first_name', prenume)
    if (nume)
    formData.append('last_name', nume);
    if (email)
    formData.append('email', email);
    if (telefon)
    formData.append('phone', telefon);
    if (this.fileToUpload)
    formData.append('cv', this.fileToUpload, this.fileToUpload.name);
    if (mesaj)
    formData.append('description', mesaj);
    if (this.data.id) {
    formData.append('job', this.data.id.toString());
    }


    this.apiService.saveApplication(formData).subscribe(() => {
      this.router.navigate(['/joburi']);
      this.dialogRef.close();
    })
  }

  handleFileInput(e: any) {
    const files = e?.target?.files?.[0];
    this.fileToUpload = files;
  }

}
