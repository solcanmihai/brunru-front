import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Application } from './contact/contact.component';
import { Category } from './jobs/jobs.component';
  import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // private basePath: string = 'http://localhost:8000/api'

  apiEndpoint = environment.apiEndpoint;


  constructor(private http: HttpClient) { }

  getJob(id: number) {
    return this.get(`/jobs/${id}`);
  }

  getCategories() {
    return this.get('/categories/');
  }

  getJobs(category_id: number | undefined) {
    return this.get(`/jobs/${category_id ? `?category=${category_id}` : ''}`);
  }

  saveApplication(application: FormData) {
    return this.http.post<any>(`${this.apiEndpoint}/application/`, application)
  }

  get(path: string) {
    return this.http.get<any>(this.apiEndpoint + path, );
  }
}
