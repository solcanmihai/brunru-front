import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Category } from './jobs/jobs.component';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  selectedCategory$ = new BehaviorSubject<Category | undefined>(undefined);
  selectedCategoryId: number | undefined;

  constructor() {
    console.log('init');
   }
}
