import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';
import { ApiService } from '../api.service';
import { Job } from '../jobs/jobs.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApplicationFormComponent } from '../application-form/application-form.component';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.scss']
})
export class JobComponent implements OnInit {

  constructor(private apiService: ApiService, private activatedRoute: ActivatedRoute, private dialog: MatDialog) { }

  job$: Observable<Job> | undefined;

  ngOnInit(): void {
    this.job$ = this.activatedRoute.params.pipe(
      filter((params) => !!params.id),
      map((params) => params.id),
      mergeMap((id) => this.apiService.getJob(id))
    );
  }

  openDialog(job: Job): void {
    const dialogRef = this.dialog.open(ApplicationFormComponent, {
      width: '650px',
      height: '700px',
      data: job 
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
