import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

export interface Application {
  id?: number,
  job_id?: number,
  cv?: File,
  first_name: string,
  last_name: string,
  phone: string,
  email: string,
  description: string,
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  fileToUpload: any;

  constructor(private apiService: ApiService, private router: Router) { }

  ngOnInit(): void {
  }

  sendMessage(prenume: string, nume: string, email: string, telefon: string, mesaj: string
  ) {
    const formData = new FormData();
    if (prenume)
    formData.append('first_name', prenume)
    if (nume)
    formData.append('last_name', nume);
    if (email)
    formData.append('email', email);
    if (telefon)
    formData.append('phone', telefon);
    if (this.fileToUpload)
    formData.append('cv', this.fileToUpload, this.fileToUpload.name);
    if (mesaj)
    formData.append('description', mesaj);


    this.apiService.saveApplication(formData).subscribe(() => {
      this.router.navigate(['/']);
    })
  }

  handleFileInput(e: any) {
    const files = e?.target?.files?.[0];
    this.fileToUpload = files;
  }
}
