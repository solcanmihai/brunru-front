import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { mergeMap, delay } from 'rxjs/operators';
import { FilterService } from '../filter.service';

export interface Category {
  name: string,
  id: number,
  icon: string,
}

export interface Job {
  // [prop: string]: any
  id: number,
  title: string,
  description: string,
  category: Category,
  benefits: string,
  requirements: string,
}

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {

  constructor(public apiService: ApiService, public filterService: FilterService) {
  }

  categories$: Observable<Category[]> | undefined;
  jobs$: Observable<Job[]> | undefined;

  ngOnInit(): void {
    this.categories$ = this.apiService.getCategories();
    this.jobs$ = this.filterService.selectedCategory$.pipe(
      mergeMap((category) => this.apiService.getJobs(category?.id))
    );
  }

  filterByCategory(category: Category | undefined) {
    this.filterService.selectedCategory$.next(category);
    this.filterService.selectedCategoryId = category?.id;
  }
}
