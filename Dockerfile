FROM node:alpine as build

WORKDIR /app

COPY . /app/

RUN npm install

RUN npm run build


FROM nginx:alpine

COPY --from=build /app/dist/brunru /usr/share/nginx/html
COPY --from=build /app/nginx/default.conf /etc/nginx/conf.d/default.conf

